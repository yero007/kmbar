package com.yero.kmbar.managers;

import java.util.ArrayList;
import java.util.List;

public class ManageDeliveredOrderId {
    private static ManageDeliveredOrderId instance;
    private List<Integer> orderIds;
    private ManageDeliveredOrderId() {
        orderIds = new ArrayList<>();
    }

    public synchronized static ManageDeliveredOrderId getInstance() {
        if(instance == null) {
            instance = new ManageDeliveredOrderId();
        }
        return instance;
    }

    public List<Integer> getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(List<Integer> orderIds) {
        this.orderIds = orderIds;
    }

    public void addOrderId(int orderId){
        orderIds.add(orderId);
    }

}
