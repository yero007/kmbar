package com.yero.kmbar.managers;

import com.yero.kmbar.data.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 14.05.2016.
 */
public class ManageCategories {
    private static ManageCategories instance;
    private List<Category> categories;
    private ManageCategories() {
        categories = new ArrayList<>();
    }

    public synchronized static ManageCategories getInstance() {
        if(instance == null) {
            instance = new ManageCategories();
        }
        return instance;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
