package com.yero.kmbar.managers;

import com.yero.kmbar.data.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 14.05.2016.
 */
public class ManageProducts {
    private static ManageProducts instance;
    private List<Product> products;

    private ManageProducts() {
        products = new ArrayList<>();
    }

    public static synchronized ManageProducts getInstance() {
        if(instance == null) {
            instance = new ManageProducts();
        }
        return instance;
    }

    public List<Product> getProducts() {
        return products;
    }

    public List<Product> getProductsByCategoryId(int categoryId) {
        List<Product> filteredProducts = new ArrayList<>();

        for(Product product : products) {
            if(product.getCategoryId() == categoryId) {
                filteredProducts.add(product);
            }
        }

        return filteredProducts;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Product getProductById(int id) {

        for (Product product : products){
            if (product.getId() == id){
                return product;
            }
        }

        return null;
    }
}
