package com.yero.kmbar.managers;

import com.yero.kmbar.data.Order;
import com.yero.kmbar.data.OrderProduct;
import com.yero.kmbar.data.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 14.05.2016.
 */
public class ManageCart {
    private static ManageCart instance;
    private List<OrderProduct> products;

    private ManageCart() {
        products = new ArrayList<>();
    }

    public synchronized static ManageCart getInstance() {
        if (instance == null) {
            instance = new ManageCart();
        }
        return instance;
    }

    public List<OrderProduct> getProducts() {
        return products;
    }

    public List<Order> getOrderProducts() {
        List<Order> orderProducts = new ArrayList<>();
        for (OrderProduct product: products) {
            orderProducts.add(product);
        }

        return orderProducts;
    }

    public void setProducts(List<OrderProduct> products) {
        this.products = products;
    }

    public void addProductToCart(Product product) {
        addProductToCart(product, 1);
    }

    public void addProductToCart(Product product, int quantity) {
        for (int i = 0; i < products.size(); i++) {
            if (products.get(i).getProductId() == product.getId()) {
                int currentQuantity = products.get(i).getQuantity();
                currentQuantity += quantity;
                products.get(i).setQuantity(currentQuantity);
                return;
            }
        }

        products.add(new OrderProduct(product, quantity));
    }

    public void removeProductFromCart(int prod_id) {
        removeProductFromCart(prod_id, 1);
    }

    public void removeProductFromCart(int prod_id, int delta) {
        for (int i = 0; i < products.size(); i++) {
            if (products.get(i).getProductId() == prod_id) {
                int currentQuantity = products.get(i).getQuantity();
                if (currentQuantity - delta > 0) {
                    currentQuantity -= delta;
                    products.get(i).setQuantity(currentQuantity);
                } else {
                    products.remove(i);
                }
                return;
            }
        }
    }

    public void removeEntireProductFromCart(int prod_id) {
        for (int i = 0; i < products.size(); i++) {
            if (products.get(i).getProductId() == prod_id) {
                products.remove(i);
                return;
            }
        }
    }

    public void clear() {
        products.clear();
    }

    public int getProductsCount (){
        return products.size();
    }
}
