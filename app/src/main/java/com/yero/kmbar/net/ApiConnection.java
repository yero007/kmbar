package com.yero.kmbar.net;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.yero.kmbar.Constants;
import com.yero.kmbar.data.ObjectNameValuePair;
import com.yero.kmbar.data.Order;
import com.yero.kmbar.data.OrderProduct;

import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class ApiConnection {
    
    public static void Login(final String userName, final String password, final APICallback callback) {
        List<ObjectNameValuePair> parameters = new ArrayList<>();
        parameters.add(new ObjectNameValuePair("email",userName));
        parameters.add(new ObjectNameValuePair("password", password));

        performRequest("user", "POST", parameters, callback, "application/json", UserResponse.class, false);
    }

    public static void GetCategories(final APICallback callback) {
        List<ObjectNameValuePair> parameters = new ArrayList<>();
        performRequest("categories", "GET", parameters, callback, "application/json", CategoriesResponse.class, false);
    }

    public static void GetProducts(final APICallback callback) {
        List<ObjectNameValuePair> parameters = new ArrayList<>();
        performRequest("products", "GET", parameters, callback, "application/json", ProductsResponse.class, false);
    }

    public static void GetTable(final String codeName, final String code, final APICallback callback) {
        List<ObjectNameValuePair> parameters = new ArrayList<>();
        parameters.add(new ObjectNameValuePair(codeName, code));

        performRequest("table", "POST", parameters, callback, "application/javascript", GetTableResponse.class, false);
    }

    public static void Deliver(final int orderId, final APICallback callback) {
        List<ObjectNameValuePair> parameters = new ArrayList<>();
        performRequest("deliver/" +orderId, "GET", parameters, callback, "application/json", DeliverResponse.class, false);
    }

    public static void PlaceOrder(final int userId, final int tableId, final List<Order> orderedProducts, final APICallback callback) {
        List<ObjectNameValuePair> parameters = new ArrayList<>();

        Gson gson = new Gson();
        String orderedString = gson.toJson(orderedProducts, ArrayList.class);

        parameters.add(new ObjectNameValuePair("", orderedString));

        performRequest("order/" + userId + "/" + tableId, "POST",
                parameters, callback, "application/json", OrderResponse.class, true);
    }

    public static void RequestBill(final int tableId, final APICallback callback) {
        List<ObjectNameValuePair> parameters = new ArrayList<>();

        performRequest("bill/" + tableId, "GET", parameters, callback, "application/json", RequestBillResponse.class, false);
    }

    public static void setContentType(HttpURLConnection connection, String contentType) {
        connection.setRequestProperty("Content-Type", contentType);
    }

    public static void performRequest(final String operation,
                                      final String requestMethod,
                                      final List<ObjectNameValuePair> parameters,
                                      final APICallback callback,
                                      final String contentType,
                                      final Class<? extends Response> responseClass,
                                      final boolean isArray) {

        new AsyncTask<Void, Void, Void>() {

            Response response = null;

            @Override
            protected Void doInBackground(Void... params) {
                String request = Constants.API_URL + operation;
                try {
                    URL urlRequest = new URL(request);
                    HttpURLConnection connection = (HttpURLConnection) urlRequest.openConnection();
                    connection.setConnectTimeout(10000);

                    connection.setRequestMethod(requestMethod);

                    if (requestMethod.equals("POST")) {
                        setContentType(connection, contentType);
                        connection.setDoOutput(true);
                        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                        JSONObject jsonParam = new JSONObject();
                        if(isArray) {
                            out.write((String)parameters.get(0).getValue());
                            out.flush();
                            out.close();
                        } else {
                            if (parameters != null && parameters.size() > 0) {
                                for (ObjectNameValuePair param : parameters) {
                                    jsonParam.put(param.getName(), param.getValue());
                                }
                                Log.d("apicall", jsonParam.toString());
                                out.write(jsonParam.toString());
                                out.flush();
                                out.close();
                            }
                        }
                    }

                    InputStream responseStream;
                    int code = connection.getResponseCode();

                    if (code == 200) {
                        responseStream = connection.getInputStream();

                        try {
                            response = parseReponse(responseStream, responseClass);
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (response != null) {
                                response.setStatus("not_found");
                            }
                        }
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void res) {
                // TODO Auto-generated method stub
                super.onPostExecute(res);
                callback.OnResult(response);
            }

        }.execute((Void[]) null);

    }

    public static <T extends Response> T parseReponse(InputStream inputStream, Class<T> responseClass) {
        Gson gson = new Gson();
        Reader reader = new InputStreamReader(inputStream);
        return gson.fromJson(reader, responseClass);
    }
}
