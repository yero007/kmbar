package com.yero.kmbar.net;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 14.05.2016.
 */
public class OrderResponse extends Response{
    @SerializedName("orders_ids")
    int [] orders_ids;

    public int[] getOrders_ids() {
        return orders_ids;
    }

    public void setOrders_ids(int[] orders_ids) {
        this.orders_ids = orders_ids;
    }
}
