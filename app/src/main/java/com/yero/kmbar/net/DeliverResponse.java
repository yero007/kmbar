package com.yero.kmbar.net;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 14.05.2016.
 */
public class DeliverResponse extends Response {
    @SerializedName("delivered_order_id")
    int deliverOrderId;

    public int getDeliverOrderId() {
        return deliverOrderId;
    }

    public void setDeliverOrderId(int deliverOrderId) {
        this.deliverOrderId = deliverOrderId;
    }
}
