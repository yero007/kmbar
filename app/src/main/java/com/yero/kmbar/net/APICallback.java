package com.yero.kmbar.net;

public interface APICallback<T extends Response> {
    void OnResult(T result);
}
