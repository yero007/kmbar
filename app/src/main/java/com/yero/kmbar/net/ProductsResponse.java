package com.yero.kmbar.net;

import com.google.gson.annotations.SerializedName;
import com.yero.kmbar.data.Product;

import java.util.ArrayList;

public class ProductsResponse extends Response {

    @SerializedName("produse")
    ArrayList <Product> products;

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }
}
