package com.yero.kmbar.net;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 14.05.2016.
 */
public class GetTableResponse extends Response {
    @SerializedName("tableId")
    int tableId;

    public int getTableId() {
        return tableId;
    }

    public void setTableId(int tableId) {
        this.tableId = tableId;
    }
}
