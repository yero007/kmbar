package com.yero.kmbar.net;

import com.google.gson.annotations.SerializedName;
import com.yero.kmbar.data.BilledProduct;
import com.yero.kmbar.data.Category;

import java.util.ArrayList;

public class RequestBillResponse extends Response {

    @SerializedName("")
    ArrayList <BilledProduct> billedProducts;

    public ArrayList<BilledProduct> getCategories() {
        return billedProducts;
    }

    public void setCategories(ArrayList<BilledProduct> billedProducts) {
        this.billedProducts = billedProducts;
    }
}
