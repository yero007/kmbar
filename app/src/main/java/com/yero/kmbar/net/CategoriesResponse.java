package com.yero.kmbar.net;

import com.google.gson.annotations.SerializedName;
import com.yero.kmbar.data.Category;
import com.yero.kmbar.data.Product;

import java.util.ArrayList;

public class CategoriesResponse extends Response {

    @SerializedName("categories")
    ArrayList <Category> categories;

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }
}
