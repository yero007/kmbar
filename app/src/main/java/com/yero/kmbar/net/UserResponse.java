package com.yero.kmbar.net;

import com.google.gson.annotations.SerializedName;
import com.yero.kmbar.data.Product;

import java.util.ArrayList;

public class UserResponse extends Response {

    @SerializedName("user_id")
    int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
