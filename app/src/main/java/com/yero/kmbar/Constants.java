package com.yero.kmbar;

public class Constants {
    public static final String API_URL = "http://mobile.itec.ligaac.ro/";

    public static final String TAG_WAS_USER_LOGGED  = "wasUserLogged";
    public static final String TAG_CODE_SCANNED = "tagBarcodeScanned";
    public static final String TAG_TABLE_CODE = "tableCode";
    public static final String TAG_TABLE_ID = "tableId";
    public static final String TAG_FRAGMENT_CATEGORIES = "categoriesFragment";
    public static final String TAG_FRAGMENT_PRODUCTS = "productsFragment";
    public static final String TAG_USER_ID = "userId";
    public static final String TAG_ERROR_WRONG_CREDENTIALS = "not_found";
    public static final String TAG_PRODUCT_ID = "productId";

    public static final String TAG_SCAN_TYPE = "scanType";
    public static final String TAG_SCAN_TYPE_NFC = "nfc";
    public static final String TAG_SCAN_TYPE_QR = "qr";

    public static final String CURRENCY = "LEI";
    public static final String TAG_FRAGMENT_CART = "cartFragment";
}
