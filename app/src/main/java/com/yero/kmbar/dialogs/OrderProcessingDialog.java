package com.yero.kmbar.dialogs;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.yero.kmbar.R;


public class OrderProcessingDialog extends DialogFragment {

    public OrderProcessingDialog(){

    }

    public static OrderProcessingDialog newInstance(String title,String description) {
        OrderProcessingDialog fragment = new OrderProcessingDialog();
        Bundle args = new Bundle();
        args.putString("title",title);
        args.putString("description", description);
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_processing, container);

        TextView description = (TextView) view.findViewById(R.id.description);
        description.setText(Html.fromHtml(getArguments().getString("description")));

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(Html.fromHtml(getArguments().getString("title")));

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setTitle(getArguments().getString("title"));
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setCancelable(false);
        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return keyCode == KeyEvent.KEYCODE_BACK;

            }
        });

        return view;
    }
}
