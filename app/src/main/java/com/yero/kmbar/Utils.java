package com.yero.kmbar;

import android.content.Context;

public class Utils {
    private static Context mContext;

    public static void init(Context ctx) {
        mContext = ctx;
    }

    public static int px(float dips) {
        float DP = mContext.getResources().getDisplayMetrics().density;
        return Math.round(dips * DP);
    }
}
