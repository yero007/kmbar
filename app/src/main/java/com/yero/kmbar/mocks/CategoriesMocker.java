package com.yero.kmbar.mocks;

import com.yero.kmbar.data.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CategoriesMocker {

    public static List<Category> getMockCategories(){

        List<Category> categories = new ArrayList<>();
        for (int i=0; i< 5; i++){
            Category category = new Category();
            category.setId(i);
            category.setDescription("Category "+i);
            categories.add(category);
        }

        return categories;
    }
}
