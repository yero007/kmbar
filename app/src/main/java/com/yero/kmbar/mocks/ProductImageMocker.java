package com.yero.kmbar.mocks;

import java.util.ArrayList;
import java.util.Random;

public class ProductImageMocker {

    public static ArrayList<String> mockImages = new ArrayList<>();
    private static Random random;

    public static void init(){
        random = new Random();
        mockImages.add("http://images6.fanpop.com/image/photos/33400000/YUMMY-FAST-FOOD-fast-food-33414496-1280-720.jpg");
        mockImages.add("http://www.myvmc.com/wp-content/uploads/2013/11/selection-of-alcohol.jpg");
        mockImages.add("http://www.oreganopizzaandpasta.co.uk/wp-content/uploads/2014/10/drink3.jpg");
        mockImages.add("http://www.bonappetit.com/wp-content/uploads/2012/07/drinks-1.jpg");
    }
    public static String getRandomImage(){

        int randomPosition = random.nextInt(mockImages.size() -1);

        return mockImages.get(randomPosition);

    }
}
