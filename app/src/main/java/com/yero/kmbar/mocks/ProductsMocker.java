package com.yero.kmbar.mocks;

import com.yero.kmbar.data.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductsMocker {

    public static List<Product> getMockProducts() {

        List<Product> products = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                Product product = new Product();
                product.setId(i);
                product.setDescription("Product " + j + " from category " + i);
                product.setImage(ProductImageMocker.getRandomImage());
                product.setCategoryId(i);
                products.add(product);
            }
        }

        return products;
    }
}
