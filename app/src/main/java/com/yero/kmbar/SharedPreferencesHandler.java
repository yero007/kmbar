package com.yero.kmbar;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedPreferencesHandler {
    SharedPreferences sharedpreferences;
    String sharedPreferencesName = "KmMBar";
    @SuppressWarnings("unused")
    private Context context;

    public SharedPreferencesHandler(Context context) {
        sharedpreferences = context.getSharedPreferences(sharedPreferencesName, Context.MODE_PRIVATE);
        this.context = context;
    }

    public SharedPreferencesHandler(Context context, String name) {
        sharedpreferences = context.getSharedPreferences(sharedPreferencesName, Context.MODE_PRIVATE);
        this.context = context;
        this.sharedPreferencesName = name;
    }

    public void addPreference(String key, String value) {
        Editor editor = sharedpreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void addPreference(String key, int value) {
        Editor editor = sharedpreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public void addPreference(String key, boolean value) {
        Editor editor = sharedpreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void addPreference(String key, float value) {
        Editor editor = sharedpreferences.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public String getStringPreference(String key) {
        return sharedpreferences.getString(key, null);
    }

    public String getStringPreference(String key, String defaultValue) {
        return sharedpreferences.getString(key, defaultValue);
    }

    public int getIntPreference(String key) {
        return sharedpreferences.getInt(key, 0);
    }

    public boolean getBooleanPreference(String key) {
        return sharedpreferences.getBoolean(key, false);
    }

    public void deletePreference(String key) {
        Editor editor = sharedpreferences.edit();
        editor.remove(key);
        editor.apply();
    }


    public float getFloatPreference(String key) {


        return sharedpreferences.getFloat(key, 0);
    }
}