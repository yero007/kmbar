package com.yero.kmbar.nfc;

public interface IOnNfcScanned {
    void onCodeScanned(String code);
}
