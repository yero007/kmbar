package com.yero.kmbar.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 14.05.2016.
 */
public class OrderProduct extends Order{

    private String description;
    private float price;
    private Product product;

    private String image;

    public OrderProduct(Product product, int quantity) {
        super(product.getId(), quantity);
        this.price = product.getPrice();
        this.image = product.getImage();
        this.product = product;

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
