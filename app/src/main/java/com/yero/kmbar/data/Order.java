package com.yero.kmbar.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 14.05.2016.
 */
public class Order {

    @SerializedName("productId")
    private int productId;

    @SerializedName("quantity")
    private int quantity;

    public Order(int productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getProductId() {

        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
}
