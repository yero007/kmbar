package com.yero.kmbar.data;

import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("id")
    private int id;

    @SerializedName("description")
    private String description;

    @SerializedName("image_src_id")
    private String imageSourceId;

    @SerializedName("category_id")
    private int categoryId;

    @SerializedName("price")
    private float price = 3.50f;

    private String image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageSourceId() {
        return imageSourceId;
    }

    public void setImageSourceId(String imageSourceId) {
        this.imageSourceId = imageSourceId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
