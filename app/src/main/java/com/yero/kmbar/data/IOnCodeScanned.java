package com.yero.kmbar.data;

public interface IOnCodeScanned {
    void onCodeScanned(String code);
}
