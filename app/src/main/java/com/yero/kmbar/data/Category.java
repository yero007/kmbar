package com.yero.kmbar.data;

import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("id")
    private int id;

    @SerializedName("description")
    private String description;

    @SerializedName("image_src_id")
    private String imageSourceId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageSourceId() {
        return imageSourceId;
    }

    public void setImageSourceId(String imageSourceId) {
        this.imageSourceId = imageSourceId;
    }
}
