package com.yero.kmbar.data;

import com.google.gson.annotations.SerializedName;
import com.yero.kmbar.net.Response;

/**
 * Created by Alex on 14.05.2016.
 */
public class BilledProduct extends Response{

    @SerializedName("product_id")
    private int productId;

    @SerializedName("delivered_order_id")
    private int deliveredOrderId;

    @SerializedName("product_name")
    private String productName;

    @SerializedName("unit_price")
    private float unitPrice;

    @SerializedName("quantity")
    private int quantity;

    @SerializedName("price")
    private float price;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getDeliveredOrderId() {
        return deliveredOrderId;
    }

    public void setDeliveredOrderId(int deliveredOrderId) {
        this.deliveredOrderId = deliveredOrderId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
