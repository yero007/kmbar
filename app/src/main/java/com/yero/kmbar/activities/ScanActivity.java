package com.yero.kmbar.activities;

import android.CaptureActivity;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.yero.kmbar.Constants;
import com.yero.kmbar.R;
import com.yero.kmbar.SharedPreferencesHandler;
import com.yero.kmbar.UserConstants;
import com.yero.kmbar.data.Order;
import com.yero.kmbar.data.OrderProduct;
import com.yero.kmbar.data.Product;
import com.yero.kmbar.net.APICallback;
import com.yero.kmbar.net.ApiConnection;
import com.yero.kmbar.net.CategoriesResponse;
import com.yero.kmbar.net.DeliverResponse;
import com.yero.kmbar.net.OrderResponse;
import com.yero.kmbar.net.ProductsResponse;
import com.yero.kmbar.net.RequestBillResponse;
import com.yero.kmbar.net.Response;
import com.yero.kmbar.net.GetTableResponse;
import com.yero.kmbar.net.UserResponse;
import com.yero.kmbar.nfc.IOnNfcScanned;
import com.yero.kmbar.nfc.NfcHandler;

import java.util.LinkedList;
import java.util.List;

public class ScanActivity extends AppCompatActivity {

    private static final int CAMERA_SCAN_REQUEST_CODE = 101;
    private static final float NFC_NOT_PRESENT_ALPHA = 0.2f;

    private IOnNfcScanned iOnNfcScanned = new IOnNfcScanned() {
        @Override
        public void onCodeScanned(final String tableCode) {

            ApiConnection.GetTable("qr", UserConstants.tableCode, new APICallback() {
                @Override
                public void OnResult(Response result) {
                    GetTableResponse table = (GetTableResponse) result;

                    saveTableIdAndRedirectToHomeScreen(tableCode, Constants.TAG_SCAN_TYPE_QR, table.getTableId());

                    saveTableIdAndRedirectToHomeScreen(tableCode, Constants.TAG_SCAN_TYPE_NFC, ((GetTableResponse) result).getTableId());

                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        View scanCodeWithCameraView = findViewById(R.id.rl_scan_camera);
        if (scanCodeWithCameraView == null) {
            return;
        }

        scanCodeWithCameraView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent cameraScanActivity = new Intent(ScanActivity.this, CaptureActivity.class);
                startActivityForResult(cameraScanActivity, CAMERA_SCAN_REQUEST_CODE);
            }
        });

        View scanCodeWithNfc = findViewById(R.id.rl_scan_nfc);
        if (scanCodeWithNfc == null) {
            return;
        }

        scanCodeWithNfc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NfcHandler.handleIntent(getIntent());
                Toast.makeText(ScanActivity.this, "Scanning", Toast.LENGTH_SHORT).show();
            }
        });

        NfcAdapter nfcAdpt = NfcHandler.initialise(this, iOnNfcScanned);
        // Check if the smartphone has NFC
        if (nfcAdpt == null) {
            Toast.makeText(ScanActivity.this,
                    getString(R.string.scan_nfc_no_nfc), Toast.LENGTH_LONG).show();
            scanCodeWithNfc.setAlpha(NFC_NOT_PRESENT_ALPHA);
            return;
        }
        // Check if NFC is enabled
        if (!nfcAdpt.isEnabled()) {
            Toast.makeText(ScanActivity.this,
                    getString(R.string.scan_nfc_not_enabled), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * It's important, that the activity is in the foreground (resumed). Otherwise
         * an IllegalStateException is thrown.
         */
        if (NfcHandler.getNfcAdpt() != null) {
            NfcHandler.setupForegroundDispatch(this);
        }
    }

    @Override
    protected void onPause() {

        if (NfcHandler.getNfcAdpt() != null) {
            NfcHandler.stopForegroundDispatch(this);
        }

        super.onPause();
    }

    @Override
    protected void onNewIntent(Intent intent) {

        NfcHandler.handleIntent(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_SCAN_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                final String tableCode = data.getStringExtra(Constants.TAG_CODE_SCANNED);

                UserConstants.tableCode = tableCode;

                ApiConnection.GetTable("qr", UserConstants.tableCode, new APICallback() {
                    @Override
                    public void OnResult(Response result) {
                        GetTableResponse table = (GetTableResponse)result;

                        saveTableIdAndRedirectToHomeScreen(tableCode, Constants.TAG_SCAN_TYPE_QR, table.getTableId());

                    }
                });

            }
        }
    }

    private void saveTableIdAndRedirectToHomeScreen(String tableCode, String scanType, final int tableId) {

        Log.d(Constants.TAG_TABLE_CODE, String.valueOf(tableCode));

        SharedPreferencesHandler sharedPreferencesHandler = new SharedPreferencesHandler(ScanActivity.this);
        sharedPreferencesHandler.addPreference(Constants.TAG_WAS_USER_LOGGED, true);
        sharedPreferencesHandler.addPreference(Constants.TAG_SCAN_TYPE, scanType);
        sharedPreferencesHandler.addPreference(Constants.TAG_TABLE_CODE, tableCode);
        sharedPreferencesHandler.addPreference(Constants.TAG_TABLE_ID, tableId);

        Intent categoriesIntent = new Intent(ScanActivity.this, MainActivity.class);
        startActivity(categoriesIntent);

        finish();
    }
}
