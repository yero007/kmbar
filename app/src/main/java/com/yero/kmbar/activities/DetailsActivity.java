package com.yero.kmbar.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.yero.kmbar.Constants;
import com.yero.kmbar.R;
import com.yero.kmbar.data.Product;
import com.yero.kmbar.managers.ManageCart;
import com.yero.kmbar.managers.ManageProducts;

import java.util.Locale;

public class DetailsActivity extends AppCompatActivity {

    Product currentProduct;
    TextView quantityTextView;
    private int currentProductId;
    private int quantity = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ImageView toolbarImage = (ImageView) findViewById(R.id.iv_details_product_image);

        if (getIntent().hasExtra(Constants.TAG_PRODUCT_ID)) {
            currentProductId = getIntent().getIntExtra(Constants.TAG_PRODUCT_ID, -1);
        } else {
            finish();
        }

        quantityTextView = (TextView) findViewById(R.id.tv_details_quantity);
        quantityTextView.setText(String.format("%d", quantity));

        View minusButton = findViewById(R.id.btn_details_minus);
        if (minusButton == null) {
            return;
        }

        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (quantity > 1) {
                    quantity--;
                    quantityTextView.setText(String.format("%d", quantity));
                }
            }
        });

        View plusButton = findViewById(R.id.btn_details_plus);
        if (plusButton == null) {
            return;
        }

        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                quantity++;
                quantityTextView.setText(String.format("%d", quantity));

            }
        });
        if (currentProductId == -1) {
            finish();
        }
        currentProduct = ManageProducts.getInstance().getProductById(currentProductId);

        if (currentProduct == null) {
            finish();
            return;
        }

        getSupportActionBar().setTitle(currentProduct.getDescription());
        Picasso.with(this).load(currentProduct.getImage()).into(toolbarImage);

        TextView priceTextView = (TextView) findViewById(R.id.tv_details_price);
        if(priceTextView == null){
            return;
        }
        String priceString = String.format(Locale.getDefault(), "%.2f %s", currentProduct.getPrice(), Constants.CURRENCY);
        priceTextView.setText(priceString);

        Button addToCartButton = (Button) findViewById(R.id.btn_details_add_cart);
        if (addToCartButton == null) {
            return;
        }

        addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManageCart.getInstance().addProductToCart(currentProduct, quantity);
            }
        });
    }
}
