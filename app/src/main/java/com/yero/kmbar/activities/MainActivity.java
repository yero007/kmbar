package com.yero.kmbar.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.yero.kmbar.Constants;
import com.yero.kmbar.R;
import com.yero.kmbar.SharedPreferencesHandler;
import com.yero.kmbar.data.Category;
import com.yero.kmbar.data.OrderProduct;
import com.yero.kmbar.data.Product;
import com.yero.kmbar.fragments.CartFragment;
import com.yero.kmbar.fragments.CategoryFragment;
import com.yero.kmbar.fragments.ProductFragment;
import com.yero.kmbar.managers.ManageCart;
import com.yero.kmbar.managers.ManageProducts;
import com.yero.kmbar.net.APICallback;
import com.yero.kmbar.net.ApiConnection;
import com.yero.kmbar.net.OrderResponse;
import com.yero.kmbar.net.Response;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, CategoryFragment.OnListFragmentInteractionListener,
        ProductFragment.OnListFragmentInteractionListener, CartFragment.OnListFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        CategoryFragment categoryFragment = CategoryFragment.newInstance(1);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_main, categoryFragment, Constants.TAG_FRAGMENT_CATEGORIES);

        ft.commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem cartItem = menu.findItem(R.id.action_cart);
        View cartView = cartItem.getActionView();

        cartItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                CartFragment cartFragment = CartFragment.newInstance(1);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.content_main, cartFragment, Constants.TAG_FRAGMENT_CART);
                ft.addToBackStack(Constants.TAG_FRAGMENT_CART);
                ft.commit();

                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_payment) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_payment) {

        } else if (id == R.id.nav_cart) {

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onListFragmentInteraction(Category item) {
        ProductFragment productFragment = ProductFragment.newInstance(2, item.getId());
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_main, productFragment, Constants.TAG_FRAGMENT_PRODUCTS);
        ft.addToBackStack(Constants.TAG_FRAGMENT_PRODUCTS);
        ft.commit();
    }

    @Override
    public void onListFragmentInteraction(Product item, View imageView, View buttonView) {
        Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
        intent.putExtra(Constants.TAG_PRODUCT_ID, item.getId());


        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                // the context of the activity
                MainActivity.this,

                // For each shared element, add to this method a new Pair item,
                // which contains the reference of the view we are transitioning *from*,
                // and the value of the transitionName attribute
                new Pair<>(imageView, getString(R.string.transition_name_image)),
                new Pair<>(buttonView, getString(R.string.transition_name_add_to_cart_button))
        );

        ActivityCompat.startActivity(MainActivity.this, intent, options.toBundle());
    }

    @Override
    public void onProductAddedToCart(Product item) {

        ManageCart.getInstance().addProductToCart(item);
        Log.d("item", "" + item.getDescription());
    }

    @Override
    public void onListFragmentInteraction(OrderProduct item) {

    }
}
