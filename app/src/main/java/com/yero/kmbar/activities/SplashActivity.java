package com.yero.kmbar.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.yero.kmbar.Constants;
import com.yero.kmbar.R;
import com.yero.kmbar.SharedPreferencesHandler;
import com.yero.kmbar.UserConstants;
import com.yero.kmbar.Utils;
import com.yero.kmbar.data.Product;
import com.yero.kmbar.managers.ManageCategories;
import com.yero.kmbar.managers.ManageProducts;
import com.yero.kmbar.mocks.CategoriesMocker;
import com.yero.kmbar.mocks.ProductImageMocker;
import com.yero.kmbar.mocks.ProductsMocker;
import com.yero.kmbar.net.APICallback;
import com.yero.kmbar.net.ApiConnection;
import com.yero.kmbar.net.CategoriesResponse;
import com.yero.kmbar.net.ProductsResponse;
import com.yero.kmbar.net.Response;

public class SplashActivity extends AppCompatActivity {

    boolean wasUserLogged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        SharedPreferencesHandler sharedPreferencesHandler = new SharedPreferencesHandler(this);
        wasUserLogged = sharedPreferencesHandler.getBooleanPreference(Constants.TAG_WAS_USER_LOGGED);

        Utils.init(this);
        ProductImageMocker.init();

        //initMocks();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    navigateToProperScreen();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }

    private void initMocks() {
        ManageCategories.getInstance().setCategories(CategoriesMocker.getMockCategories());
        ManageProducts.getInstance().setProducts(ProductsMocker.getMockProducts());

        Intent loginActivity = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(loginActivity);
    }

    private void navigateToProperScreen() {

        ApiConnection.GetCategories(new APICallback() {

            @Override
            public void OnResult(Response result) {
                if (result == null) {
                    finish();
                    return;
                }

                CategoriesResponse categoriesResponse = (CategoriesResponse) result;
                ManageCategories.getInstance().setCategories(categoriesResponse.getCategories());

                ApiConnection.GetProducts(new APICallback() {
                    @Override
                    public void OnResult(Response result) {
                        ProductsResponse productsResponse = (ProductsResponse) result;
                        ManageProducts.getInstance().setProducts(productsResponse.getProducts());

                        mockImagesForProducts();

                        if (wasUserLogged) {
                            final SharedPreferencesHandler sharedPreferencesHandler = new SharedPreferencesHandler(SplashActivity.this);
                            UserConstants.id = sharedPreferencesHandler.getIntPreference(Constants.TAG_USER_ID);

                            String scanType = sharedPreferencesHandler.getStringPreference(Constants.TAG_SCAN_TYPE);
                            String tableCode = sharedPreferencesHandler.getStringPreference(Constants.TAG_TABLE_CODE);
                            UserConstants.tableCode = tableCode;

                            if (tableCode == null) {
                                Intent scanIntent = new Intent(SplashActivity.this, ScanActivity.class);
                                startActivity(scanIntent);
                            } else {
                                int tableId = sharedPreferencesHandler.getIntPreference(Constants.TAG_TABLE_ID);

                                UserConstants.tableId = tableId;
                                UserConstants.scanType = scanType;

                                Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                                startActivity(mainIntent);
                            }
                        } else {
                            Intent loginActivity = new Intent(SplashActivity.this, LoginActivity.class);
                            startActivity(loginActivity);
                        }
                    }
                });

            }
        });

        finish();
    }

    private void mockImagesForProducts() {
        for (Product product : ManageProducts.getInstance().getProducts()) {
            product.setImage(ProductImageMocker.getRandomImage());
        }
    }
}
