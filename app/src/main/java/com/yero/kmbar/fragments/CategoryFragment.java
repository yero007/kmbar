package com.yero.kmbar.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yero.kmbar.Constants;
import com.yero.kmbar.R;
import com.yero.kmbar.SharedPreferencesHandler;
import com.yero.kmbar.activities.MainActivity;
import com.yero.kmbar.data.Category;
import com.yero.kmbar.data.OrderProduct;
import com.yero.kmbar.managers.ManageCategories;
import com.yero.kmbar.net.APICallback;
import com.yero.kmbar.net.ApiConnection;
import com.yero.kmbar.net.CategoriesResponse;
import com.yero.kmbar.net.DeliverResponse;
import com.yero.kmbar.net.GetTableResponse;
import com.yero.kmbar.net.OrderResponse;
import com.yero.kmbar.net.ProductsResponse;
import com.yero.kmbar.net.RequestBillResponse;
import com.yero.kmbar.net.Response;
import com.yero.kmbar.net.UserResponse;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CategoryFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    public CategoryFragment() {
    }


    public static CategoryFragment newInstance(int columnCount) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }




            recyclerView.setAdapter(new MyCategoryRecyclerViewAdapter(ManageCategories.getInstance().getCategories(), mListener));
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Category item);
    }
}
