package com.yero.kmbar.fragments;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yero.kmbar.R;
import com.yero.kmbar.data.Category;


import java.util.List;
import java.util.Locale;
import java.util.Random;

public class MyCategoryRecyclerViewAdapter extends RecyclerView.Adapter<MyCategoryRecyclerViewAdapter.ViewHolder> {

    private final List<Category> mValues;
    private final CategoryFragment.OnListFragmentInteractionListener mListener;

    public MyCategoryRecyclerViewAdapter(List<Category> items, CategoryFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_category_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mContentView.setText(mValues.get(position).getDescription());
        Random rand = new Random();
        int randomNum = rand.nextInt((100 - 1) + 1) + 1;

        if(randomNum % 3 == 2) {
            holder.mImageView.setImageResource(R.drawable.ic_categories_hamburger);
        } else if(randomNum % 3 == 1){
            holder.mImageView.setImageResource(R.drawable.ic_categories_pizza);
        } else {
            holder.mImageView.setImageResource(R.drawable.ic_categories_drinks);
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mContentView;
        public final ImageView mImageView;
        public Category mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (ImageView) view.findViewById(R.id.iv_categories_row_image);
            mContentView = (TextView) view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }

}
