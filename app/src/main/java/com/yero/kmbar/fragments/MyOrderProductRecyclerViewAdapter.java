package com.yero.kmbar.fragments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.yero.kmbar.Constants;
import com.yero.kmbar.R;
import com.yero.kmbar.data.OrderProduct;
import com.yero.kmbar.managers.ManageCart;

import java.util.List;
import java.util.Locale;

public class MyOrderProductRecyclerViewAdapter extends RecyclerView.Adapter<MyOrderProductRecyclerViewAdapter.ViewHolder> {

    private final List<OrderProduct> mValues;
    private final CartFragment.OnListFragmentInteractionListener mListener;
    private final CartFragment.OnProductRemovedListener onProductRemove;
    private final Context context;

    public MyOrderProductRecyclerViewAdapter(Context context, List<OrderProduct> items, CartFragment.OnListFragmentInteractionListener listener,
                                             CartFragment.OnProductRemovedListener onListFragmentInteractionListener) {
        mValues = items;
        mListener = listener;
        this.context = context;
        onProductRemove = onListFragmentInteractionListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_cart_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mItem = mValues.get(holder.getAdapterPosition());
        holder.mDescriptionView.setText("" + mValues.get(holder.getAdapterPosition()).getDescription());
        String priceString = String.format(Locale.getDefault(), "%.2f %s", holder.mItem.getPrice(), Constants.CURRENCY);
        holder.mPriceView.setText(priceString);
        holder.mQuantitiyView.setText(String.format("%d", holder.mItem.getQuantity()));
        Picasso.with(context).load(holder.mItem.getImage()).into(holder.mImageView);

        holder.mPlusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManageCart.getInstance().addProductToCart(holder.mItem.getProduct());
                holder.mQuantitiyView.setText(String.format("%d", holder.mItem.getQuantity()));

            }
        });

        holder.mMinusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.mItem.getQuantity() > 1) {
                    ManageCart.getInstance().removeProductFromCart(holder.mItem.getProductId());
                    holder.mQuantitiyView.setText(String.format("%d", holder.mItem.getQuantity()));
                }
            }
        });

        holder.mRemoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mValues.remove(holder.getAdapterPosition());
                onProductRemove.onProductRemovedFromCart(holder.getAdapterPosition());
            }
        });

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mDescriptionView;
        public final TextView mQuantitiyView;
        public final TextView mPriceView;
        public final View mMinusButton;
        public final View mPlusButton;
        public final View mRemoveButton;
        public final ImageView mImageView;

        public OrderProduct mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mDescriptionView = (TextView) view.findViewById(R.id.tv_row_cart_list_name);
            mQuantitiyView = (TextView) view.findViewById(R.id.tv_row_cart_list_quantity);
            mPriceView = (TextView) view.findViewById(R.id.tv_row_cart_list_price);
            mImageView = (ImageView) view.findViewById(R.id.iv_row_cart_list_image);

            mMinusButton = view.findViewById(R.id.btn_row_cart_minus);
            mPlusButton = view.findViewById(R.id.btn_row_cart_plus);
            mRemoveButton = view.findViewById(R.id.btn_row_cart_remove);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mDescriptionView.getText() + "'";
        }
    }
}
