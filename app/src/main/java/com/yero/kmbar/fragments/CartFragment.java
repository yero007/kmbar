package com.yero.kmbar.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.yero.kmbar.R;
import com.yero.kmbar.UserConstants;
import com.yero.kmbar.data.OrderProduct;
import com.yero.kmbar.dialogs.OrderProcessingDialog;
import com.yero.kmbar.managers.ManageCart;
import com.yero.kmbar.managers.ManageDeliveredOrderId;
import com.yero.kmbar.net.APICallback;
import com.yero.kmbar.net.ApiConnection;
import com.yero.kmbar.net.DeliverResponse;
import com.yero.kmbar.net.OrderResponse;
import com.yero.kmbar.net.Response;

import java.util.List;


public class CartFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final java.lang.String TAG_ORDER_PROCESSING_DIALOG = "order_processing";
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private Button orderButton;
    private OrderProcessingDialog orderProcessingDialog;

    public CartFragment() {
    }

    public static CartFragment newInstance(int columnCount) {
        CartFragment fragment = new CartFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_cart, container, false);

        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
        final List<OrderProduct> products = ManageCart.getInstance().getProducts();
        orderButton = (Button) view.findViewById(R.id.btn_cart_order);

        if (products.size() == 0) {
            showNoProductsView(view);
        } else {
            orderButton.setVisibility(View.VISIBLE);
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(getContext(), mColumnCount));
            }
            recyclerView.setAdapter(new MyOrderProductRecyclerViewAdapter(getContext(), products, mListener, new OnProductRemovedListener() {

                @Override
                public void onProductRemovedFromCart(int position) {
                    recyclerView.getAdapter().notifyItemRemoved(position);

                    Log.d("size", "" + products.size());
                    if (products.size() == 0) {
                        showNoProductsView(view);
                    } else {
                        orderButton.setVisibility(View.VISIBLE);
                    }
                }
            }));
        }


        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderProcessingDialog = OrderProcessingDialog.newInstance(getString(R.string.cart_dialog_title), getString(R.string.cart_dialog_message));
                orderProcessingDialog.show(getActivity().getFragmentManager(), TAG_ORDER_PROCESSING_DIALOG);

                ApiConnection.PlaceOrder(UserConstants.id, UserConstants.tableId, ManageCart.getInstance().getOrderProducts(), new APICallback() {
                    @Override
                    public void OnResult(Response result) {
                        String message;

                        if (result == null || result.getMessage() != "ok") {
                            message = getString(R.string.cart_order_error);
                        } else {
                            message = getString(R.string.cart_order_success);
                        }
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

                        OrderResponse response = (OrderResponse) result;
                        int[] orders = response.getOrders_ids();

                        for (int order : orders) {
                            ApiConnection.Deliver(order, new APICallback() {
                                @Override
                                public void OnResult(Response result) {
                                    DeliverResponse response1 = (DeliverResponse) result;
                                    ManageDeliveredOrderId.getInstance().addOrderId(response1.getDeliverOrderId());
                                }
                            });
                        }

                        ManageCart.getInstance().clear();
                        orderProcessingDialog.dismiss();
                        getActivity().onBackPressed();
                    }
                });
            }
        });

        return view;
    }

    private void showNoProductsView(View rootView) {
        View noProductsView = rootView.findViewById(R.id.rl_cart_no_products);
        noProductsView.setVisibility(View.VISIBLE);
        orderButton.setVisibility(View.INVISIBLE);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(OrderProduct item);
    }

    public interface OnProductRemovedListener {
        void onProductRemovedFromCart(int position);

    }
}
