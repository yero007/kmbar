package com.yero.kmbar.fragments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.yero.kmbar.Constants;
import com.yero.kmbar.R;
import com.yero.kmbar.data.Product;
import com.yero.kmbar.mocks.ProductImageMocker;

import java.util.List;
import java.util.Locale;

public class MyProductRecyclerViewAdapter extends RecyclerView.Adapter<MyProductRecyclerViewAdapter.ViewHolder> {

    private final List<Product> mValues;
    private final ProductFragment.OnListFragmentInteractionListener mListener;
    private final Context mContext;

    public MyProductRecyclerViewAdapter(Context context, List<Product> items, ProductFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_product_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mItem.setImage(ProductImageMocker.getRandomImage());

        Picasso.with(mContext).load(holder.mItem.getImage()).into(holder.mImageView);

        holder.mContentView.setText(mValues.get(position).getDescription());

        String priceString = String.format(Locale.getDefault(), "%.2f %s", holder.mItem.getPrice(), Constants.CURRENCY);
        holder.mPriceView.setText(priceString);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mItem, holder.mImageView, holder.mAddToCartButton);
                }
            }
        });

        holder.mAddToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onProductAddedToCart(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mContentView;
        public final TextView mPriceView;
        public final Button mAddToCartButton;
        public Product mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (ImageView) view.findViewById(R.id.iv_product_row_image);
            mContentView = (TextView) view.findViewById(R.id.content);
            mPriceView = (TextView) view.findViewById(R.id.tv_product_row_price);
            mAddToCartButton = (Button) view.findViewById(R.id.btn_product_row_add_cart);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
